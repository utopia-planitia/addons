{{- define "helper.dockerconfigyaml" }}
{{- if or .Values.credentials .Values.mirrorCredentials }}
auths:
{{- range .Values.credentials }}
  {{ .registry | quote }}:
    auth: {{ printf "%s:%s" .username .password | b64enc | quote }}
{{- end -}}
{{- with .Values.mirrorCredentials }}
  {{ .registry | quote }}:
    auth: {{ printf "%s:%s" .username .password | b64enc | quote }}
{{- end -}}
{{- else }}
auths: {}
{{- end }}
{{- end }}

{{- define "helper.dockerconfigjson" }}
{{- include "helper.dockerconfigyaml" . | fromYaml | toJson }}
{{- end }}
