# kubectl-convert
FROM docker.io/library/golang:1.24.1-alpine@sha256:43c094ad24b6ac0546c62193baeb3e6e49ce14d3250845d166c77c25f64b0386 AS golang

ARG TARGETARCH

RUN apk add --no-cache bash curl file git openssl
SHELL [ "/bin/bash", "-o", "pipefail", "-c" ]

# build c-lib independent static binaries
ENV CGO_ENABLED=0

ENV KUBERNETES_VERSION=v1.26.2
RUN git clone --depth 1 https://github.com/kubernetes/kubernetes.git -b ${KUBERNETES_VERSION}
RUN cd kubernetes && go install -ldflags '-s -w' ./cmd/kubectl-convert

ENV CHART_PRETTIER_VERSION=v1.2.2
RUN set -eux; \
    go install -ldflags '-s -w' "github.com/utopia-planitia/chart-prettier@${CHART_PRETTIER_VERSION:?}"

# kustomize
ENV KUSTOMIZE_VERSION=5.0.1
RUN curl -fsSL -o /usr/local/bin/install_kustomize.sh https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh && \
    chmod +x /usr/local/bin/install_kustomize.sh && \
    install_kustomize.sh ${KUSTOMIZE_VERSION} /usr/local/bin/

# helm
ENV HELM_VERSION=v3.11.2
RUN set -e; \
    curl -fsSL -o /usr/local/bin/get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3; \
    chmod +x /usr/local/bin/get_helm.sh; \
    DESIRED_VERSION=${HELM_VERSION} get_helm.sh;

# helmfile
ENV HELMFILE_VERSION=0.145.5
RUN set -e; \
    curl --fail --silent --show-error --location --max-time 120 \
        --output helmfile.tar.gz \
        "https://github.com/helmfile/helmfile/releases/download/v${HELMFILE_VERSION}/helmfile_${HELMFILE_VERSION}_linux_${TARGETARCH}.tar.gz"; \
    tar --auto-compress --directory=/usr/local/bin --extract --file=helmfile.tar.gz \
        --no-same-owner --no-same-permissions helmfile; \
    rm -f helmfile.tar.gz
