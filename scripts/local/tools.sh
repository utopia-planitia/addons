#!/usr/bin/env bash

set -euo pipefail
# set -x

SCRIPT_DIR=$( cd -- "$( dirname -- "${0}" )" &> /dev/null && pwd )


function func_build_quiet() {
    docker build -q -f ${SCRIPT_DIR}/Dockerfile ${SCRIPT_DIR}
}

function func_build() {
    docker build -f ${SCRIPT_DIR}/Dockerfile ${SCRIPT_DIR}
}

function func_cli() {
    # todo: $(pwd) VS ${SCRIPT_DIR}
    docker run -it --rm -w /wokrdir -v $(pwd):/wokrdir --entrypoint=bash $(func_build_quiet)
}

__usage="
Usage: $(basename $0) [OPTIONS]
Options:
  build             ...
  cli               ...
"

if [ -z "$*" ]
then
  echo "$__usage"
else
    if [ $1 == "--help" ] || [ $1 == "-h" ]
    then
        echo "$__usage"
    fi

    if [ $1 == "build" ]
    then
      func_build
    fi

    if [ $1 == "cli" ]
    then
      func_cli
    fi
fi
