# Addons

- Apiserver-DNS updates Cloudflare to point a subdomain to controll plain nodes
- Backups from etcd and kubeadm pki certificates are created and stored in minio
- Descheduler moves pods between nodes to ensure the same load on all nodes
- Kured reboots nodes one at a time
- Local path storage provisions local directorys as kubernetes volumes
- Node local dns cache runs on each node, intercepts DNS UDP request and forwards them as TCP
- kubernetes-replicator copies secrets between namespaces
- Secret Generator adds randoms strings to kubernetes secrets
