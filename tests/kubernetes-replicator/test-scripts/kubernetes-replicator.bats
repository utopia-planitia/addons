#!/usr/bin/env bats

set -o pipefail

export TEST_NAMESPACE=kubernetes-replicator-test
export TEST_NAMESPACE_A=kubernetes-replicator-test-a
export TEST_NAMESPACE_B=kubernetes-replicator-test-b

export TEST_SECRET=kubernetes-replicator-test
export TEST_SECRET_KEY=kubernetes-replicator-test
# printf 'kubernetes-replicator-test' | base64
export TEST_SECRET_VALUE=a3ViZXJuZXRlcy1yZXBsaWNhdG9yLXRlc3Q=
export TEST_SECRET_PATCH='{"data": {"kubernetes-replicator-test": "a3ViZXJuZXRlcy1yZXBsaWNhdG9yLXRlc3Q="}}'
export TEST_SECRET_JSON_PATH='{.data.kubernetes-replicator-test}'

export TIMEOUT=30
export SLEEP=1

secret_exists_in_default_namespace() {
  kubectl get secret "${TEST_SECRET:?}" \
    --namespace default \
    --output=name
}

wait_until_secret_exists_in_default_namespace() {
  until secret_exists_in_default_namespace; do sleep "${SLEEP:?}"; done
}

secret_exists_in_test_namespace() {
  kubectl get secret "${TEST_SECRET:?}" \
    --namespace "${TEST_NAMESPACE:?}" \
    --output=name
}

wait_until_secret_exists_in_test_namespace() {
  until secret_exists_in_test_namespace; do sleep "${SLEEP:?}"; done
}

wait_until_secret_does_not_exist_in_test_namespace() {
  while secret_exists_in_test_namespace; do sleep "${SLEEP:?}"; done
}

secret_data_in_test_namespace_has_expected_value() {
  kubectl get secret "${TEST_SECRET:?}" \
    --namespace="${TEST_NAMESPACE:?}" \
    --output=jsonpath="${TEST_SECRET_JSON_PATH}" |
    grep -F "${TEST_SECRET_VALUE:?}"
}

wait_until_secret_data_in_test_namespace_has_expected_value() {
  until secret_data_in_test_namespace_has_expected_value; do sleep "${SLEEP:?}"; done
}

export -f \
  secret_exists_in_default_namespace \
  wait_until_secret_exists_in_default_namespace \
  secret_exists_in_test_namespace \
  wait_until_secret_exists_in_test_namespace \
  wait_until_secret_does_not_exist_in_test_namespace \
  secret_data_in_test_namespace_has_expected_value \
  wait_until_secret_data_in_test_namespace_has_expected_value

@test "kubernetes-replicator copies annotated secrets to specified namespaces only" {
  run kubectl annotate "secret/${TEST_SECRET:?}" replicator.v1.mittwald.de/replicate-to="${TEST_NAMESPACE:?}" \
    --namespace "${RELEASE_NAMESPACE:?}" \
    --overwrite=true \
    --output=name
  [[ "${output}" = "secret/${TEST_SECRET:?}" ]]
  [[ "${status}" -eq 0 ]]

  timeout "${TIMEOUT:?}" bash -euxo pipefail -c wait_until_secret_exists_in_test_namespace >&2

  run kubectl --namespace "${TEST_NAMESPACE:?}" get secret "${TEST_SECRET:?}" --output=name
  [[ "${output}" = "secret/${TEST_SECRET:?}" ]]
  [[ "${status}" -eq 0 ]]

  run kubectl --namespace default get secret "${TEST_SECRET:?}" --output=name
  [[ "${status}" -ne 0 ]]
}

@test "kubernetes-replicator updates copies when the original secret content changed" {
  run kubectl get secret "${TEST_SECRET:?}" \
    --namespace="${RELEASE_NAMESPACE:?}" \
    --output=jsonpath="${TEST_SECRET_JSON_PATH:?}"
  [[ "${output}" = "" ]]
  [[ "${status}" -eq 0 ]]

  run kubectl get secret "${TEST_SECRET:?}" \
    --namespace="${TEST_NAMESPACE:?}" \
    --output=jsonpath="${TEST_SECRET_JSON_PATH:?}"
  [[ "${output}" = "" ]]
  [[ "${status}" -eq 0 ]]

  run kubectl patch secret "${TEST_SECRET:?}" \
    --namespace="${RELEASE_NAMESPACE:?}" \
    --output=name \
    --patch="${TEST_SECRET_PATCH:?}"
  [[ "${output}" = "secret/${TEST_SECRET:?}" ]]
  [[ "${status}" -eq 0 ]]

  run kubectl get secret "${TEST_SECRET:?}" \
    --namespace="${RELEASE_NAMESPACE:?}" \
    --output=jsonpath="${TEST_SECRET_JSON_PATH:?}"
  [[ "${output}" = "${TEST_SECRET_VALUE:?}" ]]
  [[ "${status}" -eq 0 ]]

  timeout "${TIMEOUT:?}" bash -euxo pipefail -c wait_until_secret_data_in_test_namespace_has_expected_value >&2

  run kubectl get secret "${TEST_SECRET:?}" \
    --namespace="${TEST_NAMESPACE:?}" \
    --output=jsonpath="${TEST_SECRET_JSON_PATH:?}"
  [[ "${output}" = "${TEST_SECRET_VALUE:?}" ]]
  [[ "${status}" -eq 0 ]]
}

# worked with kubed, does not with kubernetes-replicator
# not required for now
# @test "kubernetes-replicator deletes copies when the annotation is deleted" {
#   run kubectl annotate "secret/${TEST_SECRET:?}" "replicator.v1.mittwald.de/replicate-to-" \
#     --namespace "${RELEASE_NAMESPACE:?}" \
#     --overwrite=true \
#     --output=name
#   [[ "${output}" = "secret/${TEST_SECRET:?}" ]]
#   [[ "${status}" -eq 0 ]]
# 
#   timeout "${TIMEOUT:?}" bash -euxo pipefail -c wait_until_secret_does_not_exist_in_test_namespace >&2
# 
#   run kubectl --namespace "${TEST_NAMESPACE:?}" get secret "${TEST_SECRET:?}" --output=name
#   [[ "${status}" -ne 0 ]]
# }

@test "kubernetes-replicator copies \".*\"-annotated secrets to all none terminating namespaces" {
  # this namespace will be in the terminating phase for 30+ seconds
  run kubectl delete \
    namespace "${TEST_NAMESPACE_A:?}" \
    --output=name --wait=false
  [[ "${output}" = "namespace/${TEST_NAMESPACE_A:?}" ]]
  [[ "${status}" -eq 0 ]]

  # assert that the namespace is terminating
  run kubectl get \
    namespace "${TEST_NAMESPACE_A:?}" \
    --output=jsonpath='{.status.phase}'
  [[ "${output}" = "Terminating" ]]
  [[ "${status}" -eq 0 ]]

  # tell kubernetes-replicator to copy the secret to all namespaces
  run kubectl annotate "secret/${TEST_SECRET:?}" \
    replicator.v1.mittwald.de/replicate-to=".*" \
    --namespace "${RELEASE_NAMESPACE:?}" \
    --overwrite=true \
    --output=name
  [[ "${output}" = "secret/${TEST_SECRET:?}" ]]
  [[ "${status}" -eq 0 ]]

  timeout "${TIMEOUT:?}" bash -euxo pipefail -c wait_until_secret_exists_in_test_namespace >&2

  # assert that the secret has been copied to the test namespace
  run kubectl --namespace "${TEST_NAMESPACE:?}" get secret "${TEST_SECRET:?}" --output=name
  [[ "${output}" = "secret/${TEST_SECRET:?}" ]]
  [[ "${status}" -eq 0 ]]

  timeout "${TIMEOUT:?}" bash -euxo pipefail -c wait_until_secret_exists_in_default_namespace >&2

  # assert that the secret has been copied to the default namespace
  run kubectl --namespace default get secret "${TEST_SECRET:?}" --output=name
  [[ "${output}" = "secret/${TEST_SECRET:?}" ]]
  [[ "${status}" -eq 0 ]]
}

@test "kubernetes-replicator deletes copies when the original secret is deleted" {
  run kubectl delete "secret/${TEST_SECRET:?}" \
    --namespace "${RELEASE_NAMESPACE:?}" \
    --output=name \
    --wait
  [[ "${output}" = "secret/${TEST_SECRET:?}" ]]
  [[ "${status}" -eq 0 ]]

  timeout "${TIMEOUT:?}" bash -euxo pipefail -c wait_until_secret_does_not_exist_in_test_namespace >&2

  run kubectl --namespace "${TEST_NAMESPACE:?}" get secret "${TEST_SECRET:?}" --output=name
  [[ "${status}" -ne 0 ]]

  run kubectl --namespace default get secret "${TEST_SECRET:?}" --output=name
  [[ "${status}" -ne 0 ]]
}
