#!/bin/sh

set -o errexit
set -o nounset

DNS_IP="10.96.0.10"
DNS_IP_ESCAPED="$(echo "${DNS_IP:?}" | sed -E 's/\./[.]/g')"

DNS_SERVICE="node-local-dns"
DNS_NAMESPACE="kube-system"
DNS_DOMAIN="cluster.local"

LOOKUP_ADDRESSES="$(
  cat <<EOF
${DNS_SERVICE:?}
${DNS_SERVICE:?}.${DNS_NAMESPACE:?}.svc
${DNS_SERVICE:?}.${DNS_NAMESPACE:?}.svc.${DNS_DOMAIN:?}
example.com
EOF
)"

for LOOKUP_ADDRESS in ${LOOKUP_ADDRESSES:?}; do
  if ! nslookup "${LOOKUP_ADDRESS:?}" | tee -a /dev/stderr | grep -Eq "^Address:\s+${DNS_IP_ESCAPED:?}[#]53$"; then
    printf 'ERROR: expected to find the node-local-dns address (%s) in the output of `nslookup %s` (see above)\n' "${DNS_IP:?}" "${LOOKUP_ADDRESS:?}" >&2
    exit 1
  fi
done

exit 0
