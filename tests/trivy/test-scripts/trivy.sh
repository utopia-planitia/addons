#!/bin/sh

set -o errexit
set -o nounset

DIGEST=sha256:1304f174557314a7ed9eddb4eab12fed12cb0cd9809e4c28f29af86979a3c870
KEY=fanal::artifact::sha256:9c3c00f7623ec9d6434ca1e7b268c704ed59d285a839d22164eeba0d1f3907b0

echo "prepare: tests"
prepare_tests() (
  set -o xtrace
  if ! kubectl exec statefulset.apps/trivy -- sh -euc 'true'; then
    kubectl get pods,statefulset.apps --selector app.kubernetes.io/name=trivy
    return 1
  fi
)
if prepare_tests; then
  echo "SUCCESS"
  echo "---"
else
  echo "FAILURE"
  exit 1
fi

echo "test: trivy scans an image"
scan_image() (
  set -o xtrace
  kubectl exec statefulset.apps/trivy -- sh -euc "trivy --debug image --security-checks=vuln --server=http://trivy public.ecr.aws/docker/library/alpine@${DIGEST:?} 2>&1"
)
if OUTPUT=$(scan_image); then
  echo "SUCCESS"
  echo "---"
else
  echo "FAILURE:"
  echo "${OUTPUT:-}"
  exit 1
fi
