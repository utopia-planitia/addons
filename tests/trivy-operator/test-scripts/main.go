package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
	"time"
)

type test struct {
	description  string
	input        string
	command      string
	arguments    []string
	retryMaxTime time.Duration
}

func main() {
	var namespace string
	flag.StringVar(&namespace, "namespace", "", "name of the test namespace")
	flag.Parse()

	podName := "nginx"
	containerName := "nginx"
	containerImage := "nginx:1.21.6-alpine"

	defer func() {
		_ = cleanup(namespace, podName, containerName)
	}()

	err := prepare(namespace, podName, containerName, containerImage)
	if err != nil {
		os.Exit(1)
	}

	err = runTests(namespace, podName, containerName, containerImage)
	if err != nil {
		os.Exit(1)
	}
}

func runTests(namespace, podName, containerName, containerImage string) error {
	tests := []test{
		{
			description: "Test: The metrics server is running.",
			command:     "kubectl",
			arguments: []string{
				fmt.Sprintf("--namespace=%s", namespace),
				"exec",
				"deployment.apps/trivy-operator",
				"--",
				"wget",
				"--spider",
				"-S",
				"localhost:8080/metrics",
			},
		},
		// TODO: check that the operator creates any new vulnerability reports
	}

	for _, t := range tests {
		logCmd(t.description, t.command, t.arguments)
		var stdout, stderr string
		var err error
		retries := 0
		for start := time.Now(); retries == 0 || time.Since(start) < t.retryMaxTime; {
			stdout, stderr, err = run(t.input, t.command, t.arguments...)
			if err == nil {
				break
			}
			retries++
			time.Sleep(time.Second)
		}
		logResult(stdout, stderr, err)
		if err != nil {
			return err
		}
	}

	return nil
}

func prepare(namespace, podName, containerName, containerImage string) error {
	err := cleanup(namespace, podName, containerName)
	if err != nil {
		return err
	}

	description := "Prepare: Deploy a test pod."
	stdin := getPodManifest(podName, containerName, containerImage)
	command := "kubectl"
	args := []string{
		fmt.Sprintf("--namespace=%s", namespace),
		"apply",
		"--filename=-",
		"--overwrite=true",
		"--validate=strict",
		"--wait=false",
	}
	logCmd(description, command, args)
	stdout, stderr, err := run(stdin, command, args...)
	logResult(stdout, stderr, err)

	return err
}

func cleanup(namespace, podName, containerName string) error {
	description := "Cleanup: Delete old test artifacts if they exist."
	command := "kubectl"
	args := []string{
		fmt.Sprintf("--namespace=%s", namespace),
		"delete",
		fmt.Sprintf("pod/%s", podName),
		fmt.Sprintf("configauditreport.aquasecurity.github.io/pod-%s", podName),
		fmt.Sprintf("vulnerabilityreports.aquasecurity.github.io/pod-%s-%s", podName, containerName),
		"--ignore-not-found",
		"--wait",
		"--timeout=60s",
	}
	logCmd(description, command, args)
	stdout, stderr, err := run("", command, args...)
	logResult(stdout, stderr, err)

	return err
}

func logCmd(description, command string, args []string) {
	log.Print(description)
	log.Printf("+ %s %s\n", command, strings.Join(args, " "))
}

func logResult(stdout, stderr string, runError error) {
	if _, ok := runError.(*exec.ExitError); ok {
		// TODO: add timeout information to error message
		fmt.Printf("\n%s\n\n", strings.TrimSuffix(stderr, "\n"))
		log.Printf("FAILURE\n")
		return
	}
	if runError != nil {
		fmt.Printf("%v", runError)
		return
	}
	log.Printf("SUCCESS\n\n")
}

func run(stdin, command string, args ...string) (stdout, stderr string, err error) {
	cmd := exec.Command(command, args...)
	if stdin != "" {
		cmd.Stdin = strings.NewReader(stdin)
	}
	var stderrBuffer bytes.Buffer
	cmd.Stderr = bufio.NewWriter(&stderrBuffer)
	var stdoutBuffer bytes.Buffer
	cmd.Stdout = bufio.NewWriter(&stdoutBuffer)
	err = cmd.Run()
	return stdoutBuffer.String(), stderrBuffer.String(), err
}

func getPodManifest(podName, containerName, containerImage string) string {
	return fmt.Sprintf(`apiVersion: v1
kind: Pod
metadata:
  name: "%s"
spec:
  containers:
    - name: "%s"
      image: "%s"
`, podName, containerName, containerImage)
}
