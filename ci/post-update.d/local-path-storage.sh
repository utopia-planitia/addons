#!/bin/bash
set -euxo pipefail

LOCAL_PATH_PROVISIONER_VERSION=v0.0.31

curl -s --fail -L https://raw.githubusercontent.com/rancher/local-path-provisioner/${LOCAL_PATH_PROVISIONER_VERSION}/deploy/local-path-storage.yaml \
	| sed "s/reclaimPolicy: Delete/reclaimPolicy: Retain/g" \
	| chart-prettier --truncate local-path-storage/chart/templates
