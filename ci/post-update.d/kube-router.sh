#!/bin/bash
set -euxo pipefail

# https://github.com/cloudnativelabs/kube-router/blob/master/docs/kubeadm.md
# https://github.com/cloudnativelabs/kube-router/blob/v1.3.0/daemonset/kubeadm-kuberouter-all-features.yaml

KUBE_ROUTER_VERSION=v2.0.1

curl -s --fail -L "https://github.com/cloudnativelabs/kube-router/raw/${KUBE_ROUTER_VERSION}/daemonset/kubeadm-kuberouter-all-features.yaml" \
	| chart-prettier --truncate kube-router/chart/templates

sed -i "s/args\:/args\:\n        \- \-\-metrics-path=\/metrics/g" kube-router/chart/templates/daemonset.yaml
sed -i "s/args\:/args\:\n        \- \-\-hairpin-mode=true/g" kube-router/chart/templates/daemonset.yaml
sed -i "s/args\:/args\:\n        \- \-\-metrics-port=8585/g" kube-router/chart/templates/daemonset.yaml
sed -i "s/--run-service-proxy=true/--run-service-proxy=false/g" kube-router/chart/templates/daemonset.yaml

portmap='{\n             \"type\":\"portmap\",\n             \"capabilities\":{\n                \"snat\":true,\n                \"portMappings\":true\n             }\n          }'

sed -i "s/\"isDefaultGateway\"\:true,/\"isDefaultGateway\"\:true,\n             \"hairpinMode\"\:true,/g" kube-router/chart/templates/configmap.yaml
sed -i "s/\]/,${portmap}\]/g" kube-router/chart/templates/configmap.yaml

sed -i "s|image: docker\.io/cloudnativelabs/kube-router$|image: docker.io/cloudnativelabs/kube-router:${KUBE_ROUTER_VERSION:?}|g" kube-router/chart/templates/daemonset.yaml
