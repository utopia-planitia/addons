#!/usr/bin/env bash

set -euxo pipefail

# from: https://github.com/kubernetes/kubernetes/blob/v1.18.2/cluster/addons/dns/nodelocaldns/nodelocaldns.yaml
# config hint: https://github.com/weaveworks/eksctl/issues/542#issuecomment-484624138
# docs: https://kubernetes.io/docs/tasks/administer-cluster/nodelocaldns/

K8S_VERSION=v1.31.6

rm -f node-local-dns-cache/chart/templates/*

LOCAL_DNS="169.254.20.10"
DOMAIN="cluster.local"
KUBE_DNS="10.96.0.10"

curl --fail --location --show-error --silent "https://raw.githubusercontent.com/kubernetes/kubernetes/${K8S_VERSION:?}/cluster/addons/dns/nodelocaldns/nodelocaldns.yaml" |
  sed --regexp-extended \
    --expression="s/__PILLAR__LOCAL__DNS__/${LOCAL_DNS:?}/g" \
    --expression="s/__PILLAR__DNS__DOMAIN__/${DOMAIN:?}/g" \
    --expression="s/__PILLAR__DNS__SERVER__/${KUBE_DNS:?}/g" \
    --expression='s/__PILLAR__UPSTREAM__SERVERS__/__PILLAR__UPSTREAM__SERVERS__ {\n                force_tcp\n        }/g' \
    --expression='s/^(\s+)errors$/\1debug\n\1errors/g' |
  tee node-local-dns-cache/chart/templates/nldc.yaml

cp ci/post-update.d/node-local-dns-cache-servicemonitor.yaml node-local-dns-cache/chart/templates/

chart-prettier --stdin=false node-local-dns-cache/chart/templates
