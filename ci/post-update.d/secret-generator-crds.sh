#!/bin/bash
set -euxo pipefail

SECRET_GENERATOR_VERSION=v3.4.1

rm -f secret-generator-crds/chart/templates/*

curl -s --fail -L \
	-o secret-generator-crds/chart/templates/secretgenerator.mittwald.de_basicauths_crd.yaml \
	"https://raw.githubusercontent.com/mittwald/kubernetes-secret-generator/${SECRET_GENERATOR_VERSION}/deploy/crds/secretgenerator.mittwald.de_basicauths_crd.yaml"
curl -s --fail -L \
	-o secret-generator-crds/chart/templates/secretgenerator.mittwald.de_sshkeypairs_crd.yaml \
	"https://raw.githubusercontent.com/mittwald/kubernetes-secret-generator/${SECRET_GENERATOR_VERSION}/deploy/crds/secretgenerator.mittwald.de_sshkeypairs_crd.yaml"
curl -s --fail -L \
	-o secret-generator-crds/chart/templates/secretgenerator.mittwald.de_stringsecrets_crd.yaml \
	"https://raw.githubusercontent.com/mittwald/kubernetes-secret-generator/${SECRET_GENERATOR_VERSION}/deploy/crds/secretgenerator.mittwald.de_stringsecrets_crd.yaml"

chart-prettier --stdin=false secret-generator-crds/chart/templates
