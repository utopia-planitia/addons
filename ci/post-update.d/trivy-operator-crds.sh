#!/bin/bash

set -o errexit
set -o nounset
set -o xtrace

if set +o | grep -q 'set +o pipefail'; then
  set -o pipefail
fi

# enable POSIX mode in Bash to enforce / maintain compatibility with other shells
if set +o | grep -q 'set +o posix'; then
  set -o posix
fi

rm -fr trivy-operator-crds/chart/templates
mkdir -p trivy-operator-crds/chart/templates

helmfile \
  --selector name=trivy-operator \
  --file trivy-operator/helmfile.yaml \
  --state-values-file ../ci/cluster.yaml \
  template \
  --include-crds \
  --output-dir ../trivy-operator-crds/chart/templates \
  --output-dir-template '{{ .OutputDir }}' \
  --set trivy.serverURL=required-but-irrelevant-and-ignored-value

mv trivy-operator-crds/chart/templates/trivy-operator/crds/*.yaml trivy-operator-crds/chart/templates
rm -fr trivy-operator-crds/chart/templates/trivy-operator
