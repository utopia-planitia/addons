apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: apiserver-dns
spec:
  selector:
    matchLabels:
      app: loadbalancer-apiserver
  minReadySeconds: 5
  updateStrategy:
    rollingUpdate:
      maxUnavailable: 1
  template:
    metadata:
      labels:
        app: loadbalancer-apiserver
    spec:
      hostNetwork: true
      nodeSelector:
        node-role.kubernetes.io/control-plane: ""
      terminationGracePeriodSeconds: 150
      containers:
        - name: dnslb
          image: ghcr.io/utopia-planitia/dnslb:latest@sha256:2e19ec6813818d00289944f53a1727f2a547cbdf8599c7c6b3cedacdda60b16e
          args:
            - endpoint
            - --port=6443
            - --ipv6=false
          envFrom:
            - secretRef:
                name: apiserver-dns
          resources:
            requests:
              memory: 50Mi
              cpu: 50m
            limits:
              memory: 50Mi
              cpu: 50m
