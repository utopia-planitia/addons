apiVersion: v1
kind: Secret
metadata:
  name: apiserver-dns
type: Opaque
stringData:
  CF_API_KEY: "{{ .Values.cloudflare_key }}"
  CF_API_EMAIL: "{{ .Values.cloudflare_email }}"
  CF_ZONE: "{{ .Values.cloudflare_zone }}"
  CF_SUBDOMAIN: "{{ .Values.cloudflare_subdomain }}"
